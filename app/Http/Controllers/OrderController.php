<?php

namespace App\Http\Controllers;

use App\Order;
use Auth;
use Illuminate\Http\Request;

/** Implemented some admin features backend in case  */
class OrderController extends Controller
{
    /** get the list of orders */
    public function index()
    {
        return response()->json(Order::with(['product'])->get(),200);
    }

    /** mark a product delivered else not delivered */
    public function deliverOrder(Order $order)
    {
        $order->is_delivered = true;
        $status = $order->save();

        return response()->json([
            'status'    => $status,
            'data'      => $order,
            'message'   => $status ? 'Order Delivered' : 'Error Delivering Order'
        ]);
    }

    /** create an order  */
    public function store(Request $request)
    {
        $products = $request->only('products');
        foreach($products as $product) {
            try {
                $order = Order::create([
                    'product_id' => $product['product_id'],
                    'user_id' => Auth::id(),
                    'quantity' => $product['quantity'],
                    'address' => $request->address
                ]);
            }
            catch (\Illuminate\Database\QueryException $exception) {
                return response()->json([
                    'message' => 'Order Created'], 200);
            }
        }

        return response()->json([
            'message' => 'Order Created'], 200);
    }

    /** Show the order */
    public function show(Order $order)
    {
        return response()->json($order,200);
    }

    /** Update the order */
    public function update(Request $request, Order $order)
    {
        $status = $order->update(
            $request->only(['quantity'])
        );

        return response()->json([
            'status' => $status,
            'message' => $status ? 'Order Updated' : 'Error Updating Order'
        ]);
    }

    /** Delete an order */
    public function destroy(Order $order)
    {
        $status = $order->delete();

        return response()->json([
            'status' => $status,
            'message' => $status ? 'Order Deleted' : 'Error Deleting Order'
        ]);
    }
}