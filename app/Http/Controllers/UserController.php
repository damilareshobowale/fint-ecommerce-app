<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use Validator;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /** get the lists of all orders made by the user */
    public function index()
    {
        return response()->json(User::with(['orders'])->get());
    }

    /** handles login */
    public function login(Request $request)
    {
        $status = 401;
        $response = ['error' => 'Unauthorised'];
        if (Auth::attempt($request->only(['email', 'password']))) {
            $status = 200;
            $response = [
                'user' => Auth::user(),
                'token' => Auth::user()->createToken('fintEcom')->accessToken,
            ];
        }
        else {
            $status = 400;
            $response = ['error' => 'Email and password is invalid'];
        }

        return response()->json($response, $status);
    }

    /** handles register */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:50',
            'email' => 'required|email',
            'password' => 'required|min:6',
            'c_password' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $data = $request->only(['name', 'email', 'password']);
        $data['password'] = bcrypt($data['password']);

        try { 
            $user = User::create($data);
            $user->is_admin = 0;
        }
        catch (\Illuminate\Database\QueryException $exception) {
            return response()->json(['error' => 'Email already exists'], 400);
        }
        catch (PDOException $exception) {
            return response()->json(['error' => 'Sorry, there was an error saving your information.'], 400);
        }

        return response()->json([
            'user' => $user,
            'token' => $user->createToken('fintEcom')->accessToken,
        ]);
    }

    /** get a user info */
    public function show(User $user)
    {
        return response()->json($user);
    }

     /** show the order of a user */
    public function showOrders(User $user)
    {
        return response()->json($user->orders()->with(['product'])->get());
    }

}
