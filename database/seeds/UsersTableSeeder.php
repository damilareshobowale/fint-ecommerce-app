<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $user = new User;
        $user->name = "FINT Admin";
        $user->email = "fintng@gmail.com";
        $user->password = bcrypt('123456');
        $user->save();
    }
}
