
# About the APP

This Project is a Simple Single Page Application (SPA) that implements VueJS as Front end and Laravel as Backend. Laravel is accessible, powerful, and provides tools required for large, robust applications while VueJS is a Progressive Javascripts Framework to build powerful User Intefaces.

## How to Set up
1. Clone the repo using ```git clone  https://damilareshobowale@bitbucket.org/damilareshobowale/fint-ecommerce-app.git```
2. Run Composer Install to install laravel dependencies
3. Run ```yarn``` or ```npm install``` to install vueJS dependencies (Node must be installed)
4. Configure the database by renaming .env.example file to .env and configure your database as requested. I used MYSQL as the default to build the app
5. Make sure the database is properly connected and then run php artisan migrate --seed to migrate to database including the seeders created.
5. Then run ```npm run dev``` to build the development server.
6. The VueJS app is located under ```resources\js``` folder.
7. You ca be watching and building your files as you make changes in vue app using 'npm run watch'
9. Start php composer server with ```php artisan serve```
8. That's it, the application should be running now. -: )