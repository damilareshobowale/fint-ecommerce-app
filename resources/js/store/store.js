import Vue from 'vue';
import Vuex from 'vuex';

import auth from './modules/auth';
import product from './modules/product';
import header from './modules/header';
import cart from './modules/cart';
import orders from './modules/orders';
import wishes from './modules/wishes';

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        auth,
        product,
        header,
        cart,
        orders,
        wishes,
    }
})