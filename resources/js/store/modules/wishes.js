import * as types from '../types';
import toast from '../../services/toast';

const wishes = localStorage.getItem('fintEcom.wishes');
const wishesCount = localStorage.getItem('fintEcom.wishesCount');

const state = {
    wishes:  wishes ? JSON.parse(wishes) : [],
    wishesCount: wishesCount ? localStorage.getItem('fintEcom.wishesCount') : 0,
    alreadyWished: false

};

const mutations = { 
    'ADD_TO_WISHES' (state, data) {
    let found = state.wishes.findIndex(wish => wish.id == data.id);
    if (found !== -1) {
        state.alreadyWished = true;
    } else {
        state.wishes.push(data);
        state.wishesCount++;
        this.commit('saveWishes');
    }
    },
    'REMOVE_FROM_WISHES' (state, data) {
        let index = state.wishes.indexOf(data);

        if (index > -1) {
            let product = state.wishes[index];
            state.wishesCount--;

            state.wishes.splice(index, 1);
        }
        this.commit('saveWishes');
    },

    saveWishes(state) {
        localStorage.setItem('fintEcom.wishes', JSON.stringify(state.wishes));
        localStorage.setItem('fintEcom.wishesCount', state.wishesCount);
    },
};

const actions = {
    addtoWishes({commit}, data) {
        commit('ADD_TO_WISHES', data);
        toast.success('-:) Yay!, added to your wish lists');
    },
    removefromWishes ({commit}, data) {
        commit('REMOVE_FROM_WISHES', data);
        toast.success('-:) Uhmm!, you have removed the item from your wish lists');
    }
};

const getters = {
    wishes_count: state => {
        return state.wishesCount;
    },
    wishes: state => {
        return state.wishes;
    },

}

export default {
    state,
    mutations,
    actions,
    getters
}