import * as types from '../types';
import toast from '../../services/toast';

const state = {
    isLoading:  false,
    products: [],
    isEmpty: false,
};

const mutations = {
    'GET_ALL_PRODUCTS' (state, data) {
        state.products = data;
        state.isLoading = false;
    },
    'SET_IS_EMPTY_FALSE' (state, data) {
        state.isEmpty = true;
    }
}

const actions = {
    getAllProducts({commit}) {
    axios.get("api/products/").then(response => {
        const products = response.data;
        commit('GET_ALL_PRODUCTS', products);
    }).catch(error => {
        toast.error('Products cannot be loaded. Please try again.');
         commit('SET_IS_EMPTY_FALSE');
    });
    },
}

const getters = {
    products: state => {
        return state.products;
    },
    isLoading: state => {
        return state.isLoading;
    },
    isEmpty: state => {
        return state.isEmpty;
    },
}

export default {
    state,
    mutations,
    actions,
    getters
}
