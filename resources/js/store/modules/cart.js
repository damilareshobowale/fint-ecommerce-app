import * as types from '../types';
import toast from '../../services/toast';

const carts = localStorage.getItem(types.TOKEN_NAME_CART);
const cartCount = localStorage.getItem(types.TOKEN_NAME_CARTCOUNT);

const state = {
    cart:  carts ? JSON.parse(carts) : [],
    cartCount: cartCount ? cartCount : 0,
};

const mutations = { 
    'ADD_TO_CART' (state, data) {
    let found = state.cart.findIndex(product => product.id == data.id);
    if (found !== -1) {
        let item = state.cart[found];
        state.cart[found] = {
            ...item,
            quantity: item.quantity + 1,
            totalPrice:  (item.quantity + 1) * item.price,
        }
    } else {
        state.cart.push(data);
        Vue.set(data, 'quantity', 1);
        Vue.set(data, 'totalPrice', data.price);
    }
    state.cartCount++;
    this.commit('saveCart');
    },

    'REMOVE_FROM_CART' (state, data) {
        let index = state.cart.indexOf(data);

        if (index > -1) {
            let product = state.cart[index];
            state.cartCount -= product.quantity;

            state.cart.splice(index, 1);
        }
        this.commit('saveCart');
    },

    'UPDATE_SINGLE_CART' (state, data) {
        let index = state.cart.indexOf(data);
        if (index >= -1) {
            let product = state.cart[index];
            state.cart[index] = {
                ...product,
                quantity: parseInt(data.quantity),
                totalPrice:  parseInt(data.quantity) * product.price,
            }
            state.cartCount += parseInt(data.quantity);
        }
        this.commit('saveCart');
    },
    saveCart(state) {
        localStorage.setItem(types.TOKEN_NAME_CART, JSON.stringify(state.cart));
        localStorage.setItem(types.TOKEN_NAME_CARTCOUNT, state.cartCount);
    },
};

const actions = {
    addtoCart({commit}, data) {
        commit('ADD_TO_CART', data);
        toast.success('-:) Yay!, you added an item to your cart.');
    },
    removeFromCart({commit}, data) {
        commit('REMOVE_FROM_CART', data);
        toast.error('-:) Uhmm!, you removed this item from your cart.');
    },
    updateCartQuantity({commit}, data) {
        commit('UPDATE_SINGLE_CART', data);
        toast.error('-:) Yay!, Cart has been updated.');
    },
};

const getters = {
    cart_count: state => {
        return state.cartCount;
    },
    cart: state => {
        return state.cart;
    },

}

export default {
    state,
    mutations,
    actions,
    getters
}