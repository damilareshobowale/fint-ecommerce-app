import * as types from '../types';
import toast from '../../services/toast';

const user = localStorage.getItem(types.TOKEN_NAME_USER);
const token = localStorage.getItem(types.TOKEN_NAME_JWT);
const state = {
    token: token ? token : null,
    user: user ? JSON.parse(user) : '',
    isLoggedIn: token ? true : false,
};

const mutations = { 
    'DELETE_SESSION' (state) {
        state.token = null;
        state.user = '';
        state.isLoggedIn = false;

        this.commit('removeSession');
                   
    },
    'SET_SESSION' (state, {user, token}) {
        state.user = user;
        state.token = token;
        state.isLoggedIn = true;

        this.commit('saveSession');
    },
    'SET_ERROR' (state) {
        state.isLoggedIn = false;
    },
    saveSession(state) {
        localStorage.setItem(types.TOKEN_NAME_USER, JSON.stringify(state.user));
        localStorage.setItem(types.TOKEN_NAME_JWT, state.token);
    },
    removeSession() {
        localStorage.removeItem('fintEcom.jwt')
        localStorage.removeItem('fintEcom.user');
    } 
};

const actions = {
    deleteSession({commit}) {
        commit('DELETE_SESSION');
    },
    
    loginSession({commit}, {email, password, redirectTo}) {
        axios.post('api/login', {email, password}).then(response => {
            const data = {
                user: response.data.user,
                token: response.data.token,
            }
            commit('SET_SESSION', data);
            toast.success('Login Successful');
        }).catch(error => {
            toast.error(error.response.data.error);
        });
    },

    registerSession({commit}, {name, email, password, c_password}) {
        axios.post('api/register', {name, email, password, c_password}).then(response => {
            let user = response.data.user
            const data = {
                user: JSON.stringify(response.data.user),
                token: response.data.token,
            }
            commit('SET_SESSION', data);
            toast.success('Registration successful');
        }).catch(error => {
             commit('SET_ERROR');
            toast.error(error.response.data.error);
        });;
    },
};

const getters = {
     token: state => {
        return state.token;
    },
    user: state => {
        return state.user;
    },
    isLoggedIn: state => {
        return state.isLoggedIn;
    }
}

export default {
    state,
    mutations,
    actions,
    getters,
}