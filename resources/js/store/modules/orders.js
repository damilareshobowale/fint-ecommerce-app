import * as types from '../types';


const state = {
    orders: [],
    orderCount: 0,
    cart: localStorage.getItem(types.TOKEN_NAME_CART),
    cartCount: localStorage.getItem(types.TOKEN_NAME_CARTCOUNT),
}

const mutations = {
    'GET_ALL_ORDERS' (state, data) {
        state.orders = data;
    },
    'ADD_TO_ORDER' (state, data) {
        let found = state.cart.findIndex(product => product.id == data.id);
        if (found !== -1) {
            let product = state.cart[found];
            state.cartCount -= product.quantity;

            state.cart.splice(found, 1);
        };
        this.commit('saveCart');
    },
    saveCart(state) {
        localStorage.setItem(types.TOKEN_NAME_CART, JSON.stringify(state.cart));
        localStorage.setItem(types.TOKEN_NAME_CARTCOUNT, state.cartCount);
    },
}

const actions = {
    getOrders({commit}, data) {
        console.log(data.token);
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + data.token;

        axios.get(`api/users/${data.user.id}/orders`)
        .then(response => {
            this.orders = response.data
            commit('GET_ALL_ORDERS', response.data);
        })
    },
    addOrder({commit}, {address, user, products}) {
        axios.post('api/orders/', {address, products}).then(response => {
            products.forEach(item => {
                commit('ADD_TO_ORDER', item)
            });
            toast.success('-:) Yay!, Order has been placed as payment was successful.');
        }).catch(error => {
            toast.error(':-) Guess there was an error saving your order');
        });
    },
}

const getters = {
    orders: state => {
        return state.orders;
    },
}

export default {
    state,
    mutations,
    actions,
    getters
}