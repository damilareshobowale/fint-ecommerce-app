import Vue from 'vue';
import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/index.css';

Vue.use(VueToast);

const toast = {
    error (message, title = 'Error') {
        Vue.$toast.open({
            message,
            type: 'error',
            position: 'bottom',
            duration: 3000,
        });
    },
    success (message, title = 'Success') {
        Vue.$toast.open({
            message,
            type: 'success',
            position: 'bottom',
            duration: 3000,
        });
    }
};

export default toast;