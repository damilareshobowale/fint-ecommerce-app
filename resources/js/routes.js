import Home from './views/pages/Home'
import Login from './views/pages/auth/Login'
import Register from './views/pages/auth/Register'
import SingleProduct from './views/pages/SingleProduct'
import Checkout from './views/pages/Checkout'
import CheckoutPage from './views/pages/CheckoutPage'
import Confirmation from './views/pages/Confirmation'
import Cart from './views/pages/Cart'
import UserPage from './views/pages/UserPage'
import Wishlist from './views/pages/Wishlist'


export const routes = [
    {
        path: '/', 
        name: 'home',
        component: Home
    },
    {
        path: '/login', 
        name: 'login',
        component: Login
    },
    {
        path: '/register',
        name: 'register',
        component: Register
    },
    {
        path: '/products/:id',
        name: 'single-products',
        component: SingleProduct
    },
    {
        path: '/confirmation',
        name: 'confirmation',
        component: Confirmation
    },
    {
        path: '/cart',
        name: 'cart',
        component: Cart
    },
    {
        path: '/mycheckout',
        name: 'mycheckout',
        component: CheckoutPage,
    },
    {
        path: '/checkout',
        name: 'checkout',
        component: Checkout,
        props: (route) => ({ pid: route.query.pid })
    },
    {
        path: '/dashboard',
        name: 'userpage',
        component: UserPage,
        meta: {
            requiresAuth: true
        }
    },
    {
        path: '/wishlist', 
        name: 'wishlist',
        component: Wishlist
    }
]